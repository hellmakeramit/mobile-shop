<?php require_once 'mallick_admincp/lib/$_config.php'; session_start(); ?>
<!DOCTYPE html>
<html>
<head>
<title>Welcome To Mallick Mobile</title>
<!--/tags -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--//tags -->
<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<link rel="stylesheet" type="text/css" href="css/jquery-ui.css">
<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/font-awesome.css" rel="stylesheet"> 
<!-- //for bootstrap working -->
<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>
</head>
<body>
<!-- header -->
<?php require_once 'lib/_header.php'; ?>
<!-- //banner-top -->
<!-- Modal1 -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign In <span>Now</span></h3>
									<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
								<input type="text" name="Name" required="">
								<label>Name</label>
								<span></span>
							</div>
							<div class="styled-input">
								<input type="email" name="Email" required=""> 
								<label>Email</label>
								<span></span>
							</div> 
							<input type="submit" value="Sign In">
						</form>
						 
							<div class="clearfix"></div>
							<p><a href="#" data-toggle="modal" data-target="#myModal2" > Don't have an account?</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1">
							<img src="images/log_pic.jpg" alt=" "/>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal1 -->
<!-- Modal2 -->
		<div class="modal fade" id="myModal2" tabindex="-1" role="dialog">
			<div class="modal-dialog">
				<!-- Modal content-->
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
					</div>
						<div class="modal-body modal-body-sub_agile">
						<div class="col-md-8 modal_body_left modal_body_left1">
						<h3 class="agileinfo_sign">Sign Up <span>Now</span></h3>
									<form action="#" method="post">
							<div class="styled-input agile-styled-input-top">
								<input type="text" name="Name" required="">
								<label>Name</label>
								<span></span>
							</div>
							<div class="styled-input">
								<input type="email" name="Email" required=""> 
								<label>Email</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<input type="password" name="password" required=""> 
								<label>Password</label>
								<span></span>
							</div> 
							<div class="styled-input">
								<input type="password" name="Confirm Password" required=""> 
								<label>Confirm Password</label>
								<span></span>
							</div> 
							<input type="submit" value="Sign Up">
						</form>
						  
								<div class="clearfix"></div>
								<p><a href="#">By clicking register, I agree to your terms</a></p>

						</div>
						<div class="col-md-4 modal_body_right modal_body_right1">
							<img src="images/log_pic.jpg" alt=" "/>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<!-- //Modal content-->
			</div>
		</div>
<!-- //Modal2 -->
<!-- /banner_bottom_agile_info -->
<div class="page-head_agile_info_w3l">
		<div class="container">
		<?php 
		    $stmt = $link->query("SELECT `name` FROM `all_mobile_product` WHERE `id` = ".$_SESSION['p_id'].""); 
		    $p_name = @mysqli_fetch_assoc($stmt);
		?>
			<h3><?php echo $p_name['name']; ?><span> Mobile  </span></h3>
			<!--/w3_short--
				 <div class="services-breadcrumb">
						<div class="agile_inner_breadcrumb">

						   <ul class="w3_short">
								<li><a href="index.html">Home</a><i>|</i></li>
								<li>Men's Wear</li>
							</ul>
						 </div>
				</div>
	   <!--//w3_short-->
	</div>
</div>
<!---728x90--->
  <!-- banner-bootom-w3-agileits -->
	<div class="banner-bootom-w3-agileits">
	<div class="container">
         <!-- mens -->
		<div class="col-md-4 products-left">
			<div class="filter-price">
				<h3>Filter By <span>Price</span></h3>
					<ul class="dropdown-menu6">
						<li>                
							<div id="slider-range"></div>							
							<input type="text" id="amount" style="border: 0; color: #ffffff; font-weight: normal;" />
						</li>			
					</ul>
			</div>
			<div class="clearfix"></div>
		</div>
		<div class="col-md-8 products-right">
			<h5>All <?php echo $p_name['name']; @mysqli_free_result($stmt); ?> Mobile</h5>
			<div class="sort-grid1"></div>
			
			
			
				
				<div class="clearfix"></div>
		</div>
		<div class="clearfix"></div>
		
		<div class="single-pro">
		<?php 
		    $stmt = $link->prepare("SELECT `id`, `img1`, `img2`, `name`, `c_price`, `o_price`, `out_stk`, `new_id` FROM `all_mobile_item` WHERE `p_id` = ? ORDER BY `id` DESC");
		    $stmt->bind_param('i', $_SESSION['p_id']);
			$stmt->execute();
			$result = $stmt->get_result();
	        while($mobile = $result->fetch_assoc()){
				if($mobile['out_stk'] == 1){
		?>
		
			    <div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item" id="prolistidimg">
							<img src="<?php echo $mobile['img1']; ?>" alt="" class="pro-image-front">
							<img src="<?php echo $mobile['img2']; ?>" alt="" class="pro-image-back">
							<div class="men-cart-pro">
								<!--<div class="inner-men-cart-pro">
									<a href="#" class="link-product-add-cart" id="<?php echo $mobile['id']; ?>">Quick View</a>
								</div>-->
							</div>
							<?php if($mobile['out_stk'] == 1){
								echo "<span class=\"product-new-top\">Out Of Stock</span>";
							} ?>
							
						</div>
						<div class="item-info-product">
							<h4><a href="#"><?php echo $mobile['name']; ?></a></h4>
							<div class="info-product-price">
							<?php if(!empty($mobile['o_price'])){ ?>
								<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['o_price']; ?></span>
								<del><i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></del>
							<?php }else{ ?> 
							    <span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></span>
							<?php } ?>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
								<form action="#" method="post">
									<fieldset>
										<a href="#" class="add-carts" id="<?php echo $mobile['id']; ?>"><input type="button" name="submit" value="Book Now" class="button" /></a>
									</fieldset>
								</form>
							</div>
						</div><!-- item-info-product -->
					</div><!-- men-pro-item simpleCart_shelfItem -->
				</div><!-- col-md-3 product-men -->
				
				
				<?php }else{ 
				?>
				
				
				<div class="col-md-3 product-men">
					<div class="men-pro-item simpleCart_shelfItem">
						<div class="men-thumb-item">
							<img src="<?php echo $mobile['img1']; ?>" alt="" class="pro-image-front" height="255px" width="180px">
							<img src="<?php echo $mobile['img2']; ?>" alt="" class="pro-image-back" height="255px" width="180px">
							<div class="men-cart-pro">
								<div class="inner-men-cart-pro">
									<a href="#" class="link-product-add-cart" id="<?php echo $mobile['id']; ?>">Quick View</a>
								</div>
							</div>
							<?php if($mobile['new_id'] == 1){
								echo "<span class=\"product-new-top\">New</span>";
							} ?>
							
						</div>
						<div class="item-info-product">
							<h4><a href="#"><?php echo $mobile['name']; ?></a></h4>
							<div class="info-product-price">
							<?php if(!empty($mobile['o_price'])){ ?>
								<span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['o_price']; ?></span>
								<del><i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></del>
							<?php }else{ ?> 
							    <span class="item_price"><i class="fa fa-rupee"></i> <?php echo $mobile['c_price']; ?></span>
							<?php } ?>
							</div>
							<div class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out button2">
								<form action="#" method="post">
									<fieldset>
										<a href="#" class="add-cart" id="<?php echo $mobile['id']; ?>"><input type="button" name="submit" value="Book Now" class="button" /></a>
									</fieldset>
								</form>
							</div>
						</div><!-- item-info-product -->
					</div><!-- men-pro-item simpleCart_shelfItem -->
				</div><!-- col-md-3 product-men -->
				<?php
				} ?>			
		<?php 
			}
		?>
							
								
							<div class="clearfix"></div>
		</div><!-- single-pro -->
	</div>
</div>	
<!-- //mens -->
<!---728x90--->
<!--/grids-->
<?php require_once 'lib/_grids.php'; ?>
<!--grids-->
<!---728x90--->
<!-- footer -->
<?php require_once 'lib/_footer.php'; ?>
<!-- login -->
			<div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
				<div class="modal-dialog" role="document">
					<div class="modal-content modal-info">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>						
						</div>
						<div class="modal-body modal-spa">
							<div class="login-grids">
								<div class="login">
									<div class="login-bottom">
										<h3>Sign up for free</h3>
										<form>
											<div class="sign-up">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-up">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<h4>Re-type Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												
											</div>
											<div class="sign-up">
												<input type="submit" value="REGISTER NOW" >
											</div>
											
										</form>
									</div>
									<div class="login-right">
										<h3>Sign in with your account</h3>
										<form>
											<div class="sign-in">
												<h4>Email :</h4>
												<input type="text" value="Type here" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Type here';}" required="">	
											</div>
											<div class="sign-in">
												<h4>Password :</h4>
												<input type="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}" required="">
												<a href="#">Forgot password?</a>
											</div>
											<div class="single-bottom">
												<input type="checkbox"  id="brand" value="">
												<label for="brand"><span></span>Remember Me.</label>
											</div>
											<div class="sign-in">
												<input type="submit" value="SIGNIN" >
											</div>
										</form>
									</div>
									<div class="clearfix"></div>
								</div>
								<p>By logging in you agree to our <a href="#">Terms and Conditions</a> and <a href="#">Privacy Policy</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
<!-- //login -->
<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script src="js/responsiveslides.min.js"></script>
				<script>
						// You can also use "$(window).load(function() {"
						$(function () {
						 // Slideshow 4
						$("#slider3").responsiveSlides({
							auto: true,
							pager: true,
							nav: false,
							speed: 500,
							namespace: "callbacks",
							before: function () {
						$('.events').append("<li>before event fired.</li>");
						},
						after: function () {
							$('.events').append("<li>after event fired.</li>");
							}
							});
						});
				</script>
<script src="js/modernizr.custom.js"></script>
	<!-- Custom-JavaScript-File-Links --> 
	<!-- cart-js -->
	<script src="js/minicart.min.js"></script>
<script>
	// Mini Cart
	paypal.minicart.render({
		action: '#'
	});

	if (~window.location.search.indexOf('reset=true')) {
		paypal.minicart.reset();
	}
</script>

	<!-- //cart-js --> 
	<!---->
<style>
#loaderpro{text-align:center; background: url('loader.gif') no-repeat center; height: 150px;}
</style>
	<script type='text/javascript'>//<![CDATA[
    var frist;
	var second;	
	$(window).load(function(){
	 $( "#slider-range" ).slider({
				range: true,
				min: 2000,
				max: 100000,
				values: [ 2000, 100000 ],
				slide: function( event, ui ) { 
             	$( "#amount" ).val( "R" + ui.values[ 0 ] + " - R" + ui.values[ 1 ] );
				    frist = ui.values[ 0 ];
					second = ui.values[ 1 ];
					$('.single-pro').html('<div id="loaderpro" style="" ></div>');
					$.ajax({
						url:"product_filter.php",
						type:'post',
						data:{sprice:ui.values[ 0 ],eprice:ui.values[ 1 ]},
						success:function(result){
							$('.single-pro').html(result);
						}
					});
				}			
	 });
	$( "#amount" ).val( "R" + $( "#slider-range" ).slider( "values", 0 ) + " - R" + $( "#slider-range" ).slider( "values", 1 ) );
	});//]]>  
   
	</script>
							
						<script type="text/javascript" src="js/jquery-ui.js"></script>
					 <!---->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function($) {
		$(".scroll").click(function(event){		
			event.preventDefault();
			$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		});
	});
</script>
<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
<!-- //here ends scrolling icon -->

<!-- for bootstrap working -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<?php require_once 'lib/_all_list.php'; ?>
</body>

<!-- Mirrored from p.w3layouts.com/demos_new/template_demo/20-06-2017/elite_shoppy-demo_Free/143933984/web/mens.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Oct 2017 12:27:12 GMT -->
</html>
