-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 07, 2017 at 02:42 AM
-- Server version: 10.1.10-MariaDB
-- PHP Version: 5.5.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project_i`
--

-- --------------------------------------------------------

--
-- Table structure for table `about_us`
--

CREATE TABLE `about_us` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `txt` varchar(500) NOT NULL COMMENT 'About Us'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `about_us`
--

INSERT INTO `about_us` (`id`, `img`, `txt`) VALUES
(1, 'all_img/1069_1510191978.jpg', '<p>Lorem ipsum dolor sit amet, consectetur adipisicing elPellentesque vehicula augue eget nisl ullamcorper, molestie blandit ipsum auctor. Mauris volutpat augue dolor..</p><p>Consectetur adipisicing elit, sed do eiusmod tempor incididunt ut lab ore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco. labore et dolore magna aliqua.</p>');

-- --------------------------------------------------------

--
-- Table structure for table `all_mobile_item`
--

CREATE TABLE `all_mobile_item` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img1` varchar(100) NOT NULL COMMENT 'Image I',
  `img2` varchar(100) NOT NULL COMMENT 'Image II',
  `img3` varchar(100) NOT NULL COMMENT 'Image III',
  `name` varchar(150) NOT NULL COMMENT 'Item Name',
  `c_price` varchar(100) NOT NULL COMMENT 'Primary Price',
  `o_price` varchar(100) NOT NULL COMMENT 'Offer Price',
  `rate` varchar(100) NOT NULL COMMENT 'Rating',
  `des` text NOT NULL COMMENT 'Description',
  `info` text NOT NULL COMMENT 'Information',
  `charge` varchar(100) NOT NULL COMMENT 'Delivery Charges',
  `out_stk` int(11) NOT NULL DEFAULT '0' COMMENT 'Out Of Stock',
  `p_id` int(11) NOT NULL COMMENT 'Mobile Brand ID',
  `new_id` int(11) NOT NULL DEFAULT '0' COMMENT 'New Product'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_mobile_item`
--

INSERT INTO `all_mobile_item` (`id`, `img1`, `img2`, `img3`, `name`, `c_price`, `o_price`, `rate`, `des`, `info`, `charge`, `out_stk`, `p_id`, `new_id`) VALUES
(1, 'all_img/3458_1510678312.jpeg', 'all_img/1791_1510598600.jpg', 'all_img/3214_1510598600.jpg', 'Galaxy J2', '7390', '', '3', '<p><em><strong>Samsung Galaxy J2-2017 (Absolute black, 8 GB)&nbsp;&nbsp;(1 GB RAM)</strong></em></p><ul><li>1 GB RAM | 8 GB ROM | Expandable Upto 128 GB</li><li>4.7 inch Display</li><li>5.0MP Rear Camera | 2.0MP Front Camera</li><li>2000 mAh Battery</li><li>Cash on Delivery</li></ul>', '<p><em><strong>General</strong></em></p><ul><li><p><strong>In The Box</strong>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Handset, Battery, Travel Adapter, Stereo Headset</p></li><li><p><strong>Model Number</strong>&nbsp; &nbsp; SM-J200GTKDINS</p></li><li><p><strong>Model Name</strong>&nbsp; &nbsp; &nbsp; &nbsp; Galaxy J2-2017</p></li><li><p><strong>Color&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong>Absolute black</p></li><li><p><strong>Browse Type&nbsp; &nbsp; &nbsp; &nbsp;</strong>Smartphones</p></li><li><p><strong>SIM Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong>Dual Sim</p></li><li><p><strong>Hybrid Sim Slot&nbsp; &nbsp; </strong>Yes</p></li><li><p><strong>Touchscreen&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong>Yes</p></li><li><p><strong>OTG Compatible&nbsp; &nbsp;</strong>Yes</p></li></ul>', '200', 0, 1, 0),
(3, 'all_img/5825_1510682088.jpeg', 'all_img/3960_1510682088.jpeg', 'all_img/1776_1510682088.jpeg', 'Canvas Infinity', '9886', '', '3', '<p><em><strong>Highlights</strong></em></p><ul><li>3 GB RAM | 32 GB ROM |</li><li>5.7 inch HD Display</li><li>13MP Rear Camera | 16MP Front Camera</li><li>2900 mAh Polymer Battery</li><li>Snapdragon 425 MSM8917 Processor</li><li>Cash on Delivery</li></ul>', '<p><em><strong>General</strong></em></p><ul><li><p><strong>In The Box&nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong>Handset, Charger, Handsfree, Battery, USB Cable, User Guide, Protective Cover, Screen Guard</p></li><li><p><strong>Model Number&nbsp; &nbsp;</strong>HS2</p></li><li><p><strong>Model Name&nbsp; &nbsp; &nbsp; &nbsp;</strong>Canvas Infinity</p></li><li><p><strong>Color&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong>Black</p></li><li><p><strong>Browse Type&nbsp; &nbsp; &nbsp;&nbsp;</strong>Smartphones</p></li><li><p><strong>SIM Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</strong>Dual Sim</p></li><li><p><strong>Hybrid Sim Slot&nbsp; &nbsp;</strong>No</p></li><li><p><strong>Touchscreen&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;</strong>Yes</p></li><li><p><strong>OTG Compatible&nbsp;&nbsp;</strong>Yes</p></li><li><p><strong>Warranty Summary</strong>&nbsp; &nbsp;Brand Warranty of 1 Year Available for Mobile and 6 Months for Accessories</p></li></ul>', '100', 0, 2, 0),
(4, 'all_img/9576_1510682618.jpeg', 'all_img/3345_1510682618.jpeg', 'all_img/5843_1510682618.jpeg', 'Canvas 6', '7990', '', '3', '<p>If you are looking to upgrade your old phone to something better, take a look at this Micromax Canvas 6 smartphone. Beautifully designed, this attractive phone with a full metal unibody has a powerful processor, a fingerprint sensor and good cameras.</p><p>Designed for seamless multi-tasking, this phone is powered by a 1.3 GHz Tru-octa-core processor that also ensures launching apps are quick and smooth. It ensures a smooth gaming performance as well.</p><p>The phoneu0027s 3 GB DDR3 RAM enables you to run multi-application simultaneously without slowing down. So whether you need to send a message while you are in the middle of watching a video or launch RAM-hogging apps like Facebook or WhatsApp, the RAM ensures that everything is smooth and lag-free.</p>', '<p><em><strong>Highlights</strong></em></p><ul><li>3 GB RAM | 32 GB ROM | Expandable Upto 128 GB</li><li>5.5 inch Full HD Display</li><li>13MP Rear Camera | 8MP Front Camera</li><li>3000 mAh Li-Polymer Battery</li><li>Mediatek MT6753 Octa Core 1.3GHz Processor</li><li><p>In The Box Handset, Charger, Hands-free, USB Cable, User Guide, Warranty Card, Screen Guard, SIM Tray Gear</p></li><li><p>Cash on Delivery</p></li></ul>', '100', 0, 2, 0),
(5, 'all_img/2990_1510683048.jpeg', 'all_img/126_1510683049.jpeg', 'all_img/7244_1511022089.jpeg', 'Galaxy On Max', '16900', '', '3', '<p>Meet the Samsung Galaxy On Max with flagship camera f/1.7 - the smartphone that enhances your multimedia experience. With a 13 MP rear camera and 13 MP front camera, this smartphone takes your mobile photography to the next level. Its powerful octa-core processor, along with 4 GB RAM makes way for a lag-free multitasking experience.</p><p>The Samsung Galaxy On Max sports an impressive 13 MP front camera with a large aperture lens (f/1.9) for brighter selfies in low-light conditions. As if that wasn&rsquo;t enough, you also get a flash to light your photographs up. On top of that, you can share your selfies instantly on social media to spread the joy of your favourite moments with your near and dear ones, with just a tap.</p><p>What good is a camera if you can only use it during the day? Well, the sensor in this phone takes in more light, which is extremely important in low-light conditions. Brighter and more detailed images at night for you.</p>', '<p><em><strong>Highlights</strong></em></p><ul><li>4 GB RAM | 32 GB ROM | Expandable Upto 256 GB</li><li>5.7 inch Full HD Display</li><li>13MP Rear Camera | 13MP Front Camera</li><li>3300 mAh Battery</li><li>MediaTek MTK6757V/WL 2.39GHz, 1.69GHz, Octa Core Processor</li><li>In Box Handset, Adapter, Data Cable, Headset, SIM Ejection Pin, User Manual</li><li>Cash on Delivery</li></ul>', '100', 0, 1, 0),
(6, 'all_img/7707_1510683441.jpeg', 'all_img/7637_1510683441.jpeg', 'all_img/2297_1510683442.jpeg', 'Galaxy J5 Prime', '12990', '10990', '3', '<p>Powerful enough to multitask without breaking a sweat, stylish enough to make jaws drop, and secure enough to keep things truly private - the Samsung Galaxy S8 Plus offers you the ultimate smartphone experience.</p>', '<p><em><strong>Highlights</strong></em></p><ul><li>3 GB RAM | 32 GB ROM | Expandable Upto 256 GB</li><li>5 inch HD Display</li><li>13MP Rear Camera | 5MP Front Camera</li><li>2400 mAh Li-Ion Battery</li><li>Exynos 7570 Quad Core 1.4GHz Processor</li><li>In The Box&nbsp;Handset, Adaptor, Earphone, User Manual</li></ul>', '100', 0, 1, 0),
(7, 'all_img/5676_1510683771.jpeg', 'all_img/104_1510683771.jpeg', 'all_img/6952_1510683771.jpeg', 'Galaxy S8 Plus', '67899', '', '4', '<p>Meet the Samsung Galaxy S8 Plus - the phone that&rsquo;ll take your smartphone experience to the next level. The end-to-end Infinity Display of the Galaxy S8 Plus flows seamlessly into the aluminum shell, offering a smooth, curved surface without sharp angles. Boasting a top-notch security system featuring an Iris scanner, face recognition, fingerprint reader and more, the Galaxy S8 Plus keeps your private data safe from prying eyes. Its powerful 8 MP front camera and 12 MP rear camera further add to this smartphone&rsquo;s appeal.</p><p>Powerful enough to multitask without breaking a sweat, stylish enough to make jaws drop, and secure enough to keep things truly private - the Samsung Galaxy S8 Plus offers you the ultimate smartphone experience.</p>', '<p><em><strong>Highlights</strong></em></p><ul><li>6 GB RAM | 128 GB ROM | Expandable Upto 256 GB</li><li>6.2 inch Quad HD+ Display</li><li>12MP Rear Camera | 8MP Front Camera</li><li>3500 mAh Battery</li><li>Exynos 8895 Octa Core 2.3GHz Processor</li><li>Cash on Delivery</li><li>In The Box Handset, Earphones Tuned by AKG, USB Type-C Cable, Travel Adapter, USB Connector (C to A), Micro USB Connector (C to B), Ejector Pin, Quick Start and Smart Switch Leaflet</li></ul>', '100', 0, 1, 1),
(8, 'all_img/9320_1511075501.jpg', 'all_img/8906_1511075501.jpg', 'all_img/6802_1511075501.jpg', 'S7 ', '10000', '9500', '1', '<p>&nbsp;Demo&nbsp;</p>', '<p>Demo1</p>', '150', 0, 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `all_mobile_product`
--

CREATE TABLE `all_mobile_product` (
  `id` int(11) NOT NULL COMMENT 'Product ID',
  `name` varchar(100) NOT NULL COMMENT 'Product Name',
  `acti` int(11) NOT NULL DEFAULT '0' COMMENT 'Active Item For Index Page'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_mobile_product`
--

INSERT INTO `all_mobile_product` (`id`, `name`, `acti`) VALUES
(1, 'Samsung', 0),
(2, 'Micromax', 0),
(3, 'Sony', 0),
(4, 'oppo', 0);

-- --------------------------------------------------------

--
-- Table structure for table `all_other_item`
--

CREATE TABLE `all_other_item` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img1` varchar(100) NOT NULL COMMENT 'Image I',
  `img2` varchar(100) NOT NULL COMMENT 'Image II',
  `img3` varchar(100) NOT NULL COMMENT 'Image III',
  `name` varchar(150) NOT NULL COMMENT 'Item Name',
  `c_price` varchar(100) NOT NULL COMMENT 'Primary Price',
  `o_price` varchar(100) NOT NULL COMMENT 'Offer Price',
  `rate` varchar(100) NOT NULL COMMENT 'Rating',
  `des` text NOT NULL COMMENT 'Description',
  `info` text NOT NULL COMMENT 'Information',
  `charge` varchar(100) NOT NULL COMMENT 'Delivery Charges',
  `out_stk` int(11) NOT NULL DEFAULT '0' COMMENT 'Out Of Stock',
  `p_id` int(11) NOT NULL COMMENT 'All Accessories ID',
  `new_id` int(11) NOT NULL DEFAULT '0' COMMENT 'New Product'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_other_item`
--

INSERT INTO `all_other_item` (`id`, `img1`, `img2`, `img3`, `name`, `c_price`, `o_price`, `rate`, `des`, `info`, `charge`, `out_stk`, `p_id`, `new_id`) VALUES
(2, 'all_img/3708_1511024576.jpg', 'all_img/863_1511024576.jpg', 'all_img/8709_1511024576.jpg', 'Xiaomi TDSER02JY Headphones ', '3300', '3249', '3', '<p><em><strong>General</strong></em></p><ul><li><p>Model Name&nbsp; &nbsp; &nbsp;EA1BP</p></li><li><p>Color&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Black</p></li><li><p>Headphone&nbsp; &nbsp; &nbsp; Type On the Ear</p></li><li><p>Inline Remote&nbsp; &nbsp;No</p></li><li><p>Sales Package 1 Headphone</p></li></ul><p><em><strong>Product Details</strong></em></p><ul><li><p>Foldable/Collapsible&nbsp; &nbsp; Yes</p></li><li><p>Deep Bass&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Yes</p></li><li><p>Water Resistant&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;No</p></li><li><p>Monaural&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;No</p></li><li><p>Designed For&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Laptop, Audio Player, Studio Recording, Tablet, Television, Mobile</p></li><li><p>Magnet Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Neodymium</p></li><li><p>Driver Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Dynamic Driver</p></li><li><p>Headphone Driver Units 40 mm</p></li><li><p>Cord Type&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Flat Cable</p></li><li><p>Number of Pins&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;1</p></li></ul>', '<p><em><strong>Xiaomi Mi Headphones TDSER02JY Hi-Res Audio Relaxed Version</strong></em></p><ul><li>Design: Over the Head</li><li>Compatible With: Laptop, Audio Player, Studio Recording, Tablet, Mobile</li><li>Headphone Jack: 3.5 Flatwire</li><li>1 Year Limited Domestic Brand Warranty</li><li>10 Days Replacement Policy</li><li>Cash on Delivery available</li></ul>', '150', 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `all_other_product`
--

CREATE TABLE `all_other_product` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT 'Product Name',
  `acti` int(11) NOT NULL DEFAULT '0' COMMENT 'Active Item For Index Page'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `all_other_product`
--

INSERT INTO `all_other_product` (`id`, `name`, `acti`) VALUES
(1, 'MI Headphone', 0),
(2, 'Nokia', 0);

-- --------------------------------------------------------

--
-- Table structure for table `cms_view`
--

CREATE TABLE `cms_view` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `title` varchar(100) NOT NULL COMMENT 'Title',
  `sub_title` varchar(100) NOT NULL COMMENT 'Sub Title',
  `cat` varchar(100) NOT NULL COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cms_view`
--

INSERT INTO `cms_view` (`id`, `img`, `title`, `sub_title`, `cat`) VALUES
(1, 'all_img/1608_1511691395.jpg', 'THE BIGGEST SALE ON SAMSUNG MOBILE', 'Special for today', 'slider'),
(2, 'all_img/banner2.jpg', 'All Type Phone Sale Here', 'Come Us For Repair ', 'slider'),
(3, 'all_img/banner3.jpg', 'Enjoy Your Song', 'All Type Of Headphone Available Here', 'slider');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(100) NOT NULL COMMENT 'Heading',
  `txt` varchar(100) NOT NULL COMMENT 'Address',
  `cat` varchar(100) NOT NULL COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `txt`, `cat`) VALUES
(1, 'Phone', '9547956016', 'Header'),
(2, 'E-mail', 'mallikmobileegra786@gmail.com', 'Header'),
(3, 'Phone', '9547956016', 'Main'),
(4, 'E-mail', 'mallikmobileegra786@gmail.com', 'Main'),
(5, 'Address', 'kasba Egra, Purba madinipur, W.B. Pin- 721429', 'Main'),
(6, 'Phone', '9547956016', 'More Info'),
(7, 'E-mail', 'mallikmobileegra786@gmail.com', 'More Info'),
(8, 'Location', 'kasba Egra, Purba madinipur, W.B. Pin- 721429', 'More Info'),
(9, 'Facebook', 'facebook', 'Share'),
(10, 'Twitter', 'Twitter', 'Share'),
(11, 'Instagram', 'Instagram', 'Share'),
(12, 'Linkedin', 'Linkedin', 'Share'),
(13, 'Phone', '9547956016', 'Footer'),
(14, 'E-mail', 'mallikmobileegra786@gmail.com', 'Footer'),
(15, 'Location', 'kasba Egra, Purba madinipur, W.B. Pin- 721429', 'Footer');

-- --------------------------------------------------------

--
-- Table structure for table `cpanel`
--

CREATE TABLE `cpanel` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `username` varchar(50) NOT NULL COMMENT 'User Name',
  `password` varchar(128) NOT NULL COMMENT 'Password',
  `salt` varchar(128) NOT NULL COMMENT 'Salt '
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `cpanel`
--

INSERT INTO `cpanel` (`id`, `name`, `username`, `password`, `salt`) VALUES
(1, 'Mallick Mobile', 'admin1', 'af65e77847b078362c0390ba6ae8edca1fd0d26eb74d29ff228f8d4813393082bd82214000f0cb7a0efd1267469bdd00fe9e59151865f0b0b7e4521a02b1abf5', 'c45d017b1da6813e89cedcb6bf77c355085d50f14d963fbb193685e957264d65e8460192a6bfa9768f6bf88cc1f6b294a059198d30a842b30893abacfdecd07e');

-- --------------------------------------------------------

--
-- Table structure for table `index_tab1`
--

CREATE TABLE `index_tab1` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `head` varchar(50) NOT NULL COMMENT 'Heading',
  `txt` varchar(50) NOT NULL COMMENT 'Short Description'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_tab1`
--

INSERT INTO `index_tab1` (`id`, `img`, `head`, `txt`) VALUES
(1, 'all_img/9123_1511691663.jpg', 'FALL AHEAD', 'New Arrivals Headphone'),
(2, 'all_img/6628_1511691722.jpg', 'FALL AHEAD', 'Vivo V7 Plus');

-- --------------------------------------------------------

--
-- Table structure for table `index_tab2`
--

CREATE TABLE `index_tab2` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `head` varchar(100) NOT NULL COMMENT 'Heading',
  `txt` varchar(100) NOT NULL COMMENT 'Sub Title',
  `cust` varchar(100) NOT NULL COMMENT 'CUSTOMERS',
  `happy_client` varchar(100) NOT NULL COMMENT 'HAPPY CLIENTS',
  `service` varchar(100) NOT NULL COMMENT 'SERVICE'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `index_tab2`
--

INSERT INTO `index_tab2` (`id`, `img`, `head`, `txt`, `cust`, `happy_client`, `service`) VALUES
(1, 'all_img/8041_1511694486.jpg', 'SAVE UP TO 50% IN THIS WEEK', 'Suspendisse varius turpis efficitur erat laoreet dapibus. Mauris sollicitudin scelerisque commodo.Nu', '1653', '1523', '1523');

-- --------------------------------------------------------

--
-- Table structure for table `indx_tabs`
--

CREATE TABLE `indx_tabs` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `head` varchar(150) NOT NULL COMMENT 'Heading',
  `txt` varchar(150) NOT NULL COMMENT 'Text',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `cat` varchar(100) NOT NULL COMMENT 'Category'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `ip_address`
--

CREATE TABLE `ip_address` (
  `ip` varchar(100) NOT NULL COMMENT 'IP Address'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ip_address`
--

INSERT INTO `ip_address` (`ip`) VALUES
('127.0.0.1');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(100) NOT NULL,
  `unlock_time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `login_user`
--

CREATE TABLE `login_user` (
  `id` int(11) NOT NULL COMMENT 'Only ID',
  `user_name` varchar(100) NOT NULL COMMENT 'User ID',
  `date_time` varchar(100) NOT NULL COMMENT 'Login Date & Time'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login_user`
--

INSERT INTO `login_user` (`id`, `user_name`, `date_time`) VALUES
(4, 'Admin1', '22-06-2017 06:29 pm'),
(5, 'Admin1', '22-06-2017 06:31 pm'),
(6, 'Admin1', '22-06-2017 06:32 pm'),
(7, 'Admin1', '22-06-2017 06:32 pm'),
(8, 'Admin1', '22-06-2017 06:40 pm'),
(9, 'Admin1', '23-06-2017 10:26 am'),
(10, 'Admin1', '23-06-2017 03:16 pm'),
(11, 'Admin1', '23-06-2017 03:22 pm'),
(12, 'Admin1', '23-06-2017 03:24 pm'),
(13, 'Admin1', '23-06-2017 03:26 pm'),
(14, 'admin', '23-06-2017 05:56 pm'),
(15, 'ashish1', '23-06-2017 05:57 pm'),
(16, 'admin', '23-06-2017 05:59 pm'),
(17, 'admin1', '23-06-2017 06:00 pm'),
(18, 'admin12', '23-06-2017 06:01 pm'),
(19, 'admin1', '23-06-2017 06:02 pm'),
(20, 'admin', '23-06-2017 06:02 pm'),
(21, 'admin', '23-06-2017 06:34 pm'),
(22, 'admin1@', '23-06-2017 06:40 pm'),
(23, 'admin1@', '24-06-2017 10:46 am'),
(24, 'admin1@', '26-06-2017 10:55 am'),
(25, 'admin1@', '26-06-2017 11:38 am'),
(26, 'admin1@', '27-06-2017 10:53 am'),
(27, 'admin1@', '28-06-2017 10:37 am'),
(28, 'admin1@', '29-06-2017 10:47 am'),
(29, 'admin1@', '29-06-2017 03:23 pm'),
(30, 'admin1@', '17-07-2017 10:46 am'),
(31, 'admin1@', '17-07-2017 04:21 pm'),
(32, 'Admin1', '07-11-2017 06:58 am'),
(33, 'Admin1', '08-11-2017 06:58 am'),
(34, 'Admin1', '09-11-2017 06:03 am'),
(35, 'Admin1', '11-11-2017 10:02 am'),
(36, 'Admin1', '11-11-2017 03:57 pm'),
(37, 'Admin1', '11-11-2017 09:53 pm'),
(38, 'Admin1', '12-11-2017 08:51 am'),
(39, 'Admin1', '12-11-2017 01:09 pm'),
(40, 'Admin1', '12-11-2017 02:38 pm'),
(41, 'Admin1', '12-11-2017 04:02 pm'),
(42, 'Admin1', '12-11-2017 09:38 pm'),
(43, 'Admin1', '13-11-2017 06:50 am'),
(44, 'Admin1', '13-11-2017 10:20 pm'),
(45, 'Admin1', '13-11-2017 11:13 pm'),
(46, 'Admin1', '14-11-2017 06:43 am'),
(47, 'Admin1', '14-11-2017 09:12 pm'),
(48, 'Admin1', '15-11-2017 10:15 pm'),
(49, 'Admin1', '16-11-2017 07:06 am'),
(50, 'Admin1', '16-11-2017 10:42 pm'),
(51, 'Admin1', '17-11-2017 08:20 pm'),
(52, 'Admin1', '17-11-2017 10:32 pm'),
(53, 'Admin1', '18-11-2017 09:50 pm'),
(54, 'Admin1', '19-11-2017 12:25 pm'),
(55, 'Admin1', '19-11-2017 12:27 pm'),
(56, 'Admin1', '19-11-2017 12:29 pm'),
(57, 'Admin1', '19-11-2017 01:11 pm'),
(58, 'Admin1', '26-11-2017 12:49 pm'),
(59, 'Admin1', '26-11-2017 03:10 pm'),
(60, 'Admin1', '07-12-2017 07:10 am');

-- --------------------------------------------------------

--
-- Table structure for table `short_description`
--

CREATE TABLE `short_description` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `head` varchar(50) NOT NULL,
  `txt` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `short_description`
--

INSERT INTO `short_description` (`id`, `head`, `txt`) VALUES
(1, 'Sunday Closed', 'amet, Lorem ipsum dolor amet, Lorem ipsum dolor.'),
(2, '24/7 SUPPORT', 'Lorem ipsum dolor sit amet, consectetur'),
(3, 'MONEY BACK GUARANTEE', 'Lorem ipsum dolor sit amet, consectetur'),
(4, 'FREE GIFT COUPONS', 'Lorem ipsum dolor sit amet, consectetur');

-- --------------------------------------------------------

--
-- Table structure for table `team_member`
--

CREATE TABLE `team_member` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `img` varchar(100) NOT NULL COMMENT 'Image',
  `name` varchar(100) NOT NULL COMMENT 'Name',
  `des` varchar(100) NOT NULL COMMENT 'Designation ',
  `face` varchar(200) NOT NULL COMMENT 'Facebook',
  `twt` varchar(200) NOT NULL COMMENT 'Twitter',
  `inst` varchar(200) NOT NULL COMMENT 'Instagram',
  `links` varchar(200) NOT NULL COMMENT 'Linkedin'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `team_member`
--

INSERT INTO `team_member` (`id`, `img`, `name`, `des`, `face`, `twt`, `inst`, `links`) VALUES
(1, 'all_img/1831_1511683128.jpg', 'Joanna Vilken', 'Add Short Description', '#', '#', '#', '#');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `about_us`
--
ALTER TABLE `about_us`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_mobile_item`
--
ALTER TABLE `all_mobile_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_mobile_product`
--
ALTER TABLE `all_mobile_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_other_item`
--
ALTER TABLE `all_other_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `all_other_product`
--
ALTER TABLE `all_other_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_view`
--
ALTER TABLE `cms_view`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cpanel`
--
ALTER TABLE `cpanel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `index_tab1`
--
ALTER TABLE `index_tab1`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `index_tab2`
--
ALTER TABLE `index_tab2`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `indx_tabs`
--
ALTER TABLE `indx_tabs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_user`
--
ALTER TABLE `login_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `short_description`
--
ALTER TABLE `short_description`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `team_member`
--
ALTER TABLE `team_member`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `about_us`
--
ALTER TABLE `about_us`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `all_mobile_item`
--
ALTER TABLE `all_mobile_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `all_mobile_product`
--
ALTER TABLE `all_mobile_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Product ID', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `all_other_item`
--
ALTER TABLE `all_other_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `all_other_product`
--
ALTER TABLE `all_other_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms_view`
--
ALTER TABLE `cms_view`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `cpanel`
--
ALTER TABLE `cpanel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `index_tab1`
--
ALTER TABLE `index_tab1`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `index_tab2`
--
ALTER TABLE `index_tab2`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `indx_tabs`
--
ALTER TABLE `indx_tabs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID';
--
-- AUTO_INCREMENT for table `login_user`
--
ALTER TABLE `login_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Only ID', AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `short_description`
--
ALTER TABLE `short_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `team_member`
--
ALTER TABLE `team_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=2;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
