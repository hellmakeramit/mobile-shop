<?php
/**
 * Created by PhpStorm.
 * User: amit
 * Date: 7/20/2018
 * Time: 12:21 AM
 */
?>
<!----------------------------- banner Images  ---------------------------- -->

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->

    <div class="carousel-inner" role="listbox">
        <?php
        $cat = 'slider';
        $i = 1;
        $stmt = $link->prepare("SELECT `img`, `title`, `sub_title` FROM `cms_view` WHERE `cat` =? ORDER BY `id` ASC");
        $stmt->bind_param('s', $cat);
        $stmt->execute();
        $result = $stmt->get_result();
        while($img = $result->fetch_assoc()){
            if($i == 1){
                ?>
                <div class="item active">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3><?php echo $img['title']; ?></h3>
                            <p><?php echo $img['sub_title']; ?></p>
                            <!--<a class="hvr-outline-out button2" href="#">Book Now </a>-->
                        </div>
                    </div>
                </div>
                <?php
            }else{
                ?>
                <div class="item item<?php echo $i; ?>">
                    <div class="container">
                        <div class="carousel-caption">
                            <h3><?php echo $img['title']; ?></h3>
                            <p><?php echo $img['sub_title']; ?></p>
                            <!--<a class="hvr-outline-out button2" href="#">Book Now </a>-->
                        </div>
                    </div>
                </div>
                <?php
            }
            $i++;
        }
        ?>

    </div>
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
    <!-- The Modal -->
</div>
<!-- ------------------------------------end of banner ------------------------------------------->
