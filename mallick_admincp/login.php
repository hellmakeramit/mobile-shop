<?php 	
    require_once 'lib/$_functions.php';
sec_session_start();

if (login_check($link) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}

    $sql = $link->prepare("SELECT `unlock_time` FROM `login_attempts` ORDER BY `time` DESC LIMIT 0, 1 ");
	$sql->execute();
	$sql->bind_result($unlock_time);
	$sql->fetch();

    if(time() > $unlock_time)
	{
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <?php require_once 'lib/$_title.php'; ?>
	<script type="text/JavaScript" src="js/sha512.js"></script> 
    <script type="text/JavaScript" src="js/forms.js"></script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="plugins/iCheck/square/blue.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	<style>
	    .log{
			background-color:#ff9999;
			width:100%;
			border: 1px solid #800000;
            border-radius: 8px;
			height:30px;
		}
		.logi{
			padding:2px;
			font-family:"Comic Sans MS", cursive, sans-serif;
			font-weight:
			color:#000;
			font-size:16px;
		}
	</style>
	
  </head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Admin</b>LTE</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Sign in to start your session</p>

    <form method="post" action="valid" name="login_form">
      <div class="form-group has-feedback">
        <input type="text" name="username" class="form-control" placeholder="User Name" autocomplete="off" required />
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" name="password" id="password" placeholder="Password" class="form-control"  autocomplete="off" />
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">
        <div class="col-xs-4">
         <button type="submit" name="submit" class="btn btn-primary btn-block btn-flat" onclick="formhash(this.form, this.form.password);">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

    <!-- /.social-auth-links -->
  </div><br />
    <?php
        if($_SESSION['error'])
		{
    ?>
		    <div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Alert!</h4>
				<?php echo $_SESSION['error']; unset($_SESSION['error']); ?>
		    </div>
	<?php 
		}
		elseif($_SESSION['error_msg'])
		{
	?>
	        <div class="alert alert-danger alert-dismissible">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
				<h4><i class="icon fa fa-ban"></i> Alert!</h4>
				<?php echo $_SESSION['error_msg']; unset($_SESSION['error_msg']); ?>
		    </div>
    <?php	
		}
	?>		
   
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
<?php 
	}
   else
	{
	    header("Location:lockscreen");	
	}
	$link->close(); ?>