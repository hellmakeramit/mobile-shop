<?php
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
		require_once 'lib/_reduse.php'; 
		extract($_POST);
		if(isset($save)){
			$valid_exts = array('jpeg', 'jpg', 'JPG', 'JPEG');
			
			if(!empty($_FILES['image1']['name'])){
				
				$ext = strtolower(pathinfo($_FILES['image1']['name'], PATHINFO_EXTENSION));
				if(in_array($ext, $valid_exts)){
					if(is_file('../'.$img1))
					    unlink('../'.$img1);
				$path     = '../all_img/'.rand(1, 9999).'_'.time().'.'.$ext;		// File store in image folder
                $img_name = compress_image($_FILES["image1"]["tmp_name"], $path, 50); // Compress File in KB, (Here 10 is a percentege size of total size orginal file)
		        $img_path = explode("../", $img_name);
		        $img_path1 = $img_path[1];
				}else{
					$img_path1 = $img1;
				}
		        
			}else{
				$img_path1 = $img1;
			}
			
			if(!empty($_files['image2']['name'])){
				
				$ext = strtolower(pathinfo($_FILES['image2']['name'], PATHINFO_EXTENSION));
				if(in_array($ext, $valid_exts)){
					if(is_file('../'.$img2))
					    unlink('../'.$img2);
					
				$path     = '../all_img/'.rand(1, 9999).'_'.time().'.'.$ext;		// File store in image folder
                $img_name = compress_image($_FILES["image2"]["tmp_name"], $path, 50); // Compress File in KB, (Here 10 is a percentege size of total size orginal file)
		        $img_path = explode("../", $img_name);
		        $img_path2 = $img_path[1];
				}else{
					$img_path2 = $img2;
				}
		        
			}else{
				$img_path2 = $img2;
			}
			
			if(!empty($_FILES['image3']['name'])){
				$ext = strtolower(pathinfo($_FILES['image3']['name'], PATHINFO_EXTENSION));
				if(in_array($ext, $valid_exts)){
				
				   if(is_file('../'.$img3))
					    unlink('../'.$img3);
					    
				$path     = '../all_img/'.rand(1, 9999).'_'.time().'.'.$ext;		// File store in image folder
                $img_name = compress_image($_FILES["image3"]["tmp_name"], $path, 50); // Compress File in KB, (Here 10 is a percentege size of total size orginal file)
		        $img_path = explode("../", $img_name);
		        $img_path3 = $img_path[1];
				}else{
					$img_path3 = $img3;
				}
		        
			}else{
				$img_path3 = $img3;
			}
		 
		  $stmt = $link->prepare("UPDATE `all_mobile_item` SET `img1`=?,`img2`=?,`img3`=?,`name`=?,`c_price`=?,`o_price`=?,`rate`=?,`des`=?,`info`=?,`charge`=?,`out_stk`=?,`p_id`=? WHERE `id` = ?");
		  $stmt->bind_param('ssssssssssssi', $img_path1, $img_path2, $img_path3, $name, $c_price, $o_price, $rate, $des, $info, $charge, $out_stk, $p_id, $uid);
		  if($stmt->execute()){
				echo "<script type=\"text/javascript\">
                        alert('Successfully Update');
                        
                      </script>";
			}else{
				echo "<script type=\"text/javascript\">
                        alert('Unsuccessfully Update');
                       
                      </script>";
			}
		}
		
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <?php require_once 'lib/$_title.php'; ?>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="js/mycss.css" />
	
	<!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>-->
<link rel="stylesheet" href="css/stylesheet-image-based.css" />
<script src="//cdn.ckeditor.com/4.7.3/basic/ckeditor.js"></script>
  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php require_once 'lib/$_header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php require_once 'lib/$_menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        All Mobile List 
        <small>Mobile Product</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> All Mobile List</a></li>
        <li class="active">Mobile Product</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

        <div class="row">
			<div class="col-md-12">
			    <div class="box">
				    <div class="box-header with-border">
				        <h3 class="box-title">Edit Item</h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div><!-- /.box-header -->
						
					<div class="box-body">
					<?php 
						$stmt = $link->prepare("SELECT * FROM `all_mobile_item` WHERE `id` = ?");
						$stmt->bind_param('i', $_GET['edit']);
						$stmt->execute();
						$result = $stmt->get_result();
						$row = $result->fetch_assoc();
					?>
					     <form method="post" action="edit_mobile_list?edit=<?php echo $_GET['edit']; ?>" enctype="multipart/form-data">
					     <input type="hidden" name="uid" value="<?php echo $row['id']; ?>" />
					     <input type="hidden" name="img1" value="<?php echo $row['img1']; ?>" />
					     <input type="hidden" name="img2" value="<?php echo $row['img2']; ?>" />
					     <input type="hidden" name="img3" value="<?php echo $row['img3']; ?>" />
					     <div class="col-md-6">
					   
							   <div class="form-group">
			                    <label>Mobile Brand :</label>
			                    <select class="form-control select2" name="p_id" style="width: 100%;" required>
			                      <option value="" selected="selected">Please Select Brand</option>
			                      <?php 
			                          $mobile_brand = $link->prepare("SELECT * FROM `all_mobile_product` ORDER BY `name` ASC");
			                          $mobile_brand->execute();
			                          $result = $mobile_brand->get_result();
						              while($data = $result->fetch_assoc()){
						          ?>
						                <option value="<?php echo $data['id']; ?>" <?php if($row['p_id'] == $data['id']) echo "selected"; ?>><?php echo $data['name']; ?></option>
						          <?php    	
						              }
			                      ?>
			                    </select>
			                  </div><!-- /.form-group -->
			                  
			                  <div class="form-group">
			                    <label>Mobile Model Name :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-mobile"></i>
			                      </div>
			                      <input type="text" class="form-control" name="name" placeholder="Mobile Model Name" value="<?php echo $row['name']; ?>" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="form-group">
			                    <label>Primary Price :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="c_price" placeholder="Primary Price" value="<?php echo $row['c_price']; ?>" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="form-group">
			                    <label>Offer Price (Optional) :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="o_price" placeholder="Offer Price" value="<?php echo $row['o_price']; ?>" />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  
			             </div><!-- col-md-6 -->
			             
						 <div class="col-md-6">
						 
						    <div class="form-group">
			                    <label>Product Rating :</label>
			                    <select class="form-control select2" name="rate" style="width: 100%;" required>
			                      <option value="" selected="selected">Please Product Rating</option>
			                      <option value="1" <?php if($row['rate'] == 1) echo "selected"; ?>>1 Star</option>
			                      <option value="2" <?php if($row['rate'] == 2) echo "selected"; ?>>2 Star</option>
			                      <option value="3" <?php if($row['rate'] == 3) echo "selected"; ?>>3 Star</option>
			                      <option value="4" <?php if($row['rate'] == 4) echo "selected"; ?>>4 Star</option>
			                      <option value="5" <?php if($row['rate'] == 5) echo "selected"; ?>>5 Star</option>
			                    </select>
			                  </div><!-- /.form-group -->
			                  
			                  <div class="form-group">
			                    <label>Delivery Charges :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-rupee"></i>
			                      </div>
			                      <input type="text" class="form-control" name="charge" placeholder="Delivery Charges" value="<?php echo $row['charge']; ?>" required />
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <div class="form-group">
			                    <label>Out Of Stock :</label>
			                    <div class="input-group">
			                    <div class="example" style="height: 11px;">
			                      <div>
                                  <input id="radio1" type="radio" name="out_stk" value="0" <?php if($row['out_stk'] == 0) echo "checked"; ?>><label for="radio1"><span><span></span></span>No</label>
                                 </div>
                                 <div style="float: left; margin-left: 60px; margin-top: -28px;">
                                  <input id="radio2" type="radio" name="out_stk" value="1" <?php if($row['out_stk'] == 1) echo "checked"; ?>><label for="radio2"><span><span></span></span>Yes</label>
                                </div>
			                      </div>
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
			                  
			                  <!-- <div class="form-group">
			                    <label>Product Image (Only 3 Images) :</label>
			                    <div class="input-group">
			                      <div class="input-group-addon">
			                        <i class="fa fa-image"></i>
			                      </div>
			                      <input type="file" class="form-control" name="image[]" multiple required />
			                    </div>
			                  </div>-- /.form group -->
			                  
						 </div><!-- col-md-6 -->
						 <div class="col-md-12">
						     <div class="form-group">
			                    <label>Description :</label>
			                   
			                      <textarea class="form-control" id="editor1" name="des"><?php echo $row['des']; ?></textarea>
			                      <script>CKEDITOR.replace( 'editor1' );</script>
			                    
			                  </div><!-- /.form group -->
			                  
			                  <div class="form-group">
			                    <label>Information :</label>
			                   
			                      <textarea class="form-control" id="editor2" name="info"><?php echo $row['info']; ?></textarea>
			                      <script>CKEDITOR.replace( 'editor2' );</script>
			                    
			                  </div><!-- /.form group -->
			                  
			                 
			                  <div class="form-group" style="width: 25%; overflow: hidden;">
			                     <img src="../<?php echo $row['img1'];?>" height="100px" width="100px" /><r />
			                     <input type="file" name="image1" />
			                  </div>
			                 
			                  <div class="form-group" style="width: 25%; margin-left: 44%; margin-top: -137px;">
			                     <img src="../<?php echo $row['img2'];?>" height="100px" width="100px" /><r />
			                     <input type="file" name="image2" />
			                  </div>
			                 
			                  <div class="form-group" style="width: 15%; margin-top: -13%; margin-left: 83%;">
			                     <img src="../<?php echo $row['img3'];?>" height="100px" width="100px" /><r />
			                     <input type="file" name="image3" />
			                  </div>
			                  
			                  <button type="submit" class="btn btn-primary" name="save" style="margin-bottom: 20px;"><i class="fa fa-fw fa-save"></i> Save</button>
						 </div><!-- col-md-12 -->
						 
					     </form>
					     
					</div><!-- /.box-body -->
					  
			    </div><!-- /.box -->
            </div> <!-- /.col -->
        </div> <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require_once 'lib/$_footer.php'; ?>
</div>
<div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->
    
 <!-- jQuery 2.1.4 -->
 
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
   <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
      });
    </script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.1.0/jquery.form.min.js"></script>
	---------------------------------------Insert Data---------------------------------------------------------------------
	<script>
	    function submitform(obj){
			var name = $('#name').val();
			if(name == ''){
				alert("Name is missing");
			}else{
				$(obj).ajaxSubmit({
					success:successForm
				});
			}
			
			return false;
		}
		
		function successForm(result){
		    if(result==1){
				$('.form')[0].reset();
				$('.error').html('<i style=\"color: #05A3D3;\">Successfully Insert</i>');
			}else{
				$('.error').html('<i style=\"color:#d32205;\">Unsuccessfully Insert</i>');
			}
		}
	</script>
	
	<!-----------------------------------------Insert Data--------------------------------------------------------------------->
	<!-----------------------------------------Fetch Data---------------------------------------------------------------------
	<script type="text/javascript">
		$(document).ready(function(){
			setInterval(function(){
				$('#show').load('customer_master_data.php')
			}, 500);
		});
	</script>
	<!-----------------------------------------Fetch Data--------------------------------------------------------------------->
    <!-----------------------------------------Delete Data---------------------------------------------------------------------
	<script type="text/javascript">
	    $(function(){
			$(".delete").click(function(){
				var element = $(this);
				var userid = element.attr("id");
				var info = 'id=' + userid;
				if(confirm("Are you sure want to delete?")){
					$.ajax({
						url: 'delete_mobile_list.php',
						type: 'post',
						data: info,
						success: function(){
							
						}
					});
					$(this).parent().parent().fadeOut(1500, function(){
						$(this).remove();
					});
				};
				return false;	
			});
		});
	</script>
	
	<!-----------------------------------------Delete Data--------------------------------------------------------------------->
  </body>
</html>
<?php 
	}
	else
	{  
        @mysqli_free_result($LoginData);
		session_destroy();
		header("Location:index");
		
	}
	$link->close();
?>