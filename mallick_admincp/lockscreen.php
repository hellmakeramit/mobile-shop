<?php 
    require_once 'lib/$_functions.php';
	
	$sql = $link->query("SELECT `unlock_time` FROM `login_attempts` ORDER BY `time` DESC LIMIT 0, 1 ");
	
	$row = @mysqli_fetch_assoc($sql);
	
	$time = $row['unlock_time'] - time();
	
	unset($_SESSION['error_msg']);
	unset($_SESSION['error']);
?>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Admin Control Panel</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<style>
	    .log{
			background-color:#ff9999;
			width:100%;
			border: 1px solid #800000;
            border-radius: 8px;
			height:30px;
		}
		.logi{
			padding:2px;
			font-family:"Comic Sans MS", cursive, sans-serif;
			font-weight:
			color:#000;
			font-size:16px;
		}
	</style>
	<script>
	var seconds = <?php echo $time; ?>;
	function secondPassed() {
		var minutes = Math.round((seconds - 30)/60);
		var remainingSeconds = seconds % 60;
		if (remainingSeconds < 10) {
			remainingSeconds = "0" + remainingSeconds;  
		}
		document.getElementById('countdown').innerHTML = minutes + ":" + remainingSeconds;
		if (seconds == 0) {
			clearInterval(countdownTimer);
			window.location='index';
		} else {
			seconds--;
		}
	}
	 
	var countdownTimer = setInterval('secondPassed()', 1000);
	</script>
  </head>
  <body class="hold-transition lockscreen">
    <!-- Automatic element centering -->
    <div class="lockscreen-wrapper">
      <div class="lockscreen-logo">
        <a href="#"><b>Admin</b>LTE</a>
      </div>
      <!-- User name -->
      <div class="lockscreen-name">Admin</div>

      <!-- START LOCK SCREEN ITEM -->
      <div class="lockscreen-item">
        <!-- lockscreen image -->
        <div class="lockscreen-image">
          <img src="dist/img/avatar2.png" alt="User Image">
        </div>
        <!-- /.lockscreen-image -->

        <!-- lockscreen credentials (contains the form) -->
        <form class="lockscreen-credentials">
          <div class="input-group">
            <input type="password" class="form-control" placeholder="password" disabled />
            <div class="input-group-btn">
              <button class="btn"><i class="fa fa-arrow-right text-muted"></i></button>
            </div>
          </div>
        </form><!-- /.lockscreen credentials -->

      </div><!-- /.lockscreen-item -->
      <div class="help-block text-center">
        Your Admin Panel Unlock In <span id="countdown" class="timer"></span>
      </div>
      <div class="text-center">
        <a href="#">Or sign in as a different user</a>
      </div>
      <div class="lockscreen-footer text-center">
        Copyright &copy; 2016-<?php echo date('Y'); ?> <b><a href="#" class="text-black">Admin Panel</a></b><br>
        All rights reserved
      </div>
    </div><!-- /.center -->

    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
  </body>
</html>
<?php @mysqli_free_result($sql);


	sec_session_start();

// Unset all session values 
    $_SESSION = array();

// get session parameters 
   $params = session_get_cookie_params();

// Delete the actual cookie. 
   setcookie(session_name(),'', time() - 42000, $params["path"], $params["domain"], $params["secure"], $params["httponly"]);

// Destroy session 
   session_destroy();

 $link->close(); ?>
