<?php
require_once 'lib/$_config.php';
require_once 'lib/$_functions.php';

sec_session_start(); // Our custom secure way of starting a PHP session.

$dt = new DateTime("Asia/Kolkata");
$date1= $dt->format('d-m-Y h:i a'); 
		 
if (isset($_POST['username'], $_POST['p'])) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $password = $_POST['p']; // The hashed password.
	
    if (login($username, $password, $link) == true) {
        //Login success
		$_SESSION['username'] = $username;
		$stmt1 = $link->prepare("INSERT INTO `login_user`(`user_name`, `date_time`) VALUES (?, ?)");
		$stmt1->bind_param('ss', $username, $date1);
		$stmt1->execute(); 
		$stmt1->close();
        header("Location: home");
        exit();
    } else {
         //Login failed 
		$_SESSION['error'] = "Invalid Login";
  
        $cont = 1;
        echo $_SESSION['conter'] = $_SESSION['conter']+$cont;
          if($_SESSION['conter'] > 3)
          {
            $_SESSION['conter'] = 0;
            header("Location:lockscreen");
          }
else{
header('Location:login');
}

        //exit();
    }
} else {
    //The correct POST variables were not sent to this page.
   $_SESSION['error'] = "Could not process login";	
    header('Location:login');
    exit();
}
?>