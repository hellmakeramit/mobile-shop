<?php
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
		extract($_GET);
		
		if(isset($updt_cus))
		{
			$_SESSION['updt_cus'] = $updt_cus;
			
			Header("Location:edit_customer_data");
		}
		
		if(isset($view_item))
		{
			$_SESSION['view_item'] = $view_item;
			
			Header("Location:view_item");
		}
		
		if(isset($edit_item))
		{
			$_SESSION['edit_item'] = $edit_item;
			
			Header("Location:edit_item");
		}
		
		if(isset($view_distributor))
		{
			$_SESSION['view_distributor'] = $view_distributor;
			
			Header("Location:view_distributor");
		}
		
		if(isset($edit_distributor))
		{
			$_SESSION['edit_distributor'] = $edit_distributor;
			
			Header("Location:edit_distributor");
		}
		
		if(isset($edit_distributor_item))
		{
			$_SESSION['edit_distributor_item'] = $edit_distributor_item;
			
			Header("Location:edit_distributor_item");
		}
    }
	else
	{  
        @mysqli_free_result($LoginData);
		session_destroy();
		header("Location:index");
		
	}
	$link->close();