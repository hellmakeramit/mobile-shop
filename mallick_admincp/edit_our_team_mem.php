<?php
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
		require_once 'lib/_reduse.php'; 
		extract($_POST);
		if(isset($save)){
			if(!empty($_FILES['image']['name'])){
				$valid_exts = array('jpeg', 'jpg', 'JPG', 'JPEG');
				$ext = strtolower(pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION));
				if (in_array($ext, $valid_exts))
				{	
			        if(is_file('../'.$uimg))
					    unlink('../'.$uimg);	
					
                    $path     = '../all_img/'.rand(1, 9999).'_'.time().'.'.$ext;		// File store in image folder
                    $img_name = compress_image($_FILES["image"]["tmp_name"], $path, 50); // Compress File in KB, (Here 10 is a percentege size of total size orginal file)
				    $img_path = explode("../", $img_name);
					$stmt = $link->prepare("UPDATE `team_member` SET `img`=?,`name`=?,`des`=?,`face`=?,`twt`=?,`inst`=?,`links`=? WHERE `id` = ?");
				    $stmt->bind_param('sssssssi', $img_path[1], $name, $des, $face, $twt, $inst, $links, $uid);
					if($stmt->execute()){
						echo "<script type=\"text/javascript\">
		                        alert('Successfully Update'); 
		                      </script>";
					}else{
						echo "<script type=\"text/javascript\">
		                        alert('Unsuccessfully Update');
		                      </script>";
					}
				}else{
					echo "<script type=\"text/javascript\">
		                        alert('Invalid Image'); 
		                      </script>";
				}
			}else{
				$stmt = $link->prepare("UPDATE `team_member` SET `name`=?,`des`=?,`face`=?,`twt`=?,`inst`=?,`links`=? WHERE `id` = ?");
				$stmt->bind_param('ssssssi', $name, $des, $face, $twt, $inst, $links, $uid);
				if($stmt->execute()){
					echo "<script type=\"text/javascript\">
							alert('Successfully Update'); 
						  </script>";
				}else{
					echo "<script type=\"text/javascript\">
							alert('Unsuccessfully Update'); 
						  </script>";
				}
			}
			
		}
		
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <?php require_once 'lib/$_title.php'; ?>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="js/mycss.css" />
	
	<!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>-->

  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php require_once 'lib/$_header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php require_once 'lib/$_menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Our Team Member
        <small>CMS View</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> CMS View</a></li>
        <li class="active">Our Team Member</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

        <div class="row">
			<div class="col-md-12">
			    <div class="box">
				    <div class="box-header with-border">
				        <h3 class="box-title">Edit Member &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="error"></span></h3>
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div><!-- /.box-header -->
					<?php 
					    $stmt = $link->prepare("SELECT * FROM `team_member` WHERE `id` = ?");
						$stmt->bind_param('i', $_GET['edit_item']);
						$stmt->execute();
						$result = $stmt->get_result();
						$res = $result->fetch_assoc();
					?>	
					<div class="box-body">
					    <div class="col-md-12">
						<form method="post" action="edit_our_team_mem?edit_item=<?php echo $_GET['edit_item']; ?>" class="form" enctype="multipart/form-data">
						<input type="hidden" name="uid" value="<?php echo $res['id']; ?>" />
						<input type="hidden" name="uimg" value="<?php echo $res['img']; ?>" />
					    <div class="col-md-6">
						    <div class="form-group">
								<label>Member Image :</label>
								<div class="input-group">
                                    <img src="../<?php echo $res['img']; ?>" height="100px" width="100px" />
								    <input type="file" name="image" placeholder="Image" />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div class="form-group">
								<label>Name :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-pencil"></i>
								    </div>
								    <input type="text" class="form-control" name="name" placeholder="Name" value="<?php echo $res['name']; ?>" required />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div class="form-group">
								<label>Designation :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-pencil"></i>
								    </div>
								    <input type="text" class="form-control" name="des" placeholder="Designation" value="<?php echo $res['des']; ?>" required />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div>
								<button type="submit" name="save" class="btn btn-primary"><i class="fa fa-fw fa-save"></i> Save</button>
							</div>
							<br />
						</div>
						
						
						<div class="col-md-6">
							
							<div class="form-group">
								<label>Facebook URL :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-facebook"></i>
								    </div>
								    <input type="text" class="form-control" name="face" placeholder="Facebook" value="<?php echo $res['face']; ?>" />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div class="form-group">
								<label>Instagram URL :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-instagram"></i>
								    </div>
								    <input type="text" class="form-control" name="inst" placeholder="Instagram" value="<?php echo $res['inst']; ?>" />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div class="form-group">
								<label>Linkedin URL :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-linkedin"></i>
								    </div>
								    <input type="text" class="form-control" name="links" placeholder="Linkedin" value="<?php echo $res['links']; ?>" />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
							<div class="form-group">
								<label>Twitter URL :</label>
								<div class="input-group">
                                    <div class="input-group-addon">
									    <i class="fa fa-twitter"></i>
								    </div>
								    <input type="text" class="form-control" name="twt" placeholder="Twitter" value="<?php echo $res['twt']; ?>" />
								</div><!-- /.input group -->
							</div><!-- /.form group -->
							
						</div>
						
						</form>	
						</div>
						
					</div><!-- /.box-body -->
					  
			    </div><!-- /.box -->
            </div> <!-- /.col -->
        </div> <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require_once 'lib/$_footer.php'; ?>
</div>
<div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->
    
 <!-- jQuery 2.1.4 -->
 
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    </script>
    <!-- page script -->
    
  </body>
</html>
<?php 
	}
	else
	{  
        @mysqli_free_result($LoginData);
		session_destroy();
		header("Location:index");
		
	}
	$link->close();
?>