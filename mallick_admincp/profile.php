<?php
    //require_once 'mail/class.phpmailer.php';

	/*if(isset($_POST['updt']))
	{        
                $email  = $_POST['email'];
				$username = $_POST['username'];
				$pass = $_POST['password'];
				
		        $msg ='<p>Vriddhi Landmart Password Change</p>';
                $msg.="<p>User Name = ".$username."</p>";
                $msg.="<p>Password = ".$pass."</p>";
				
                $mail             = new PHPMailer();				
				$body             = $msg;	
				$to               = $email;
       			
				$mail->IsSMTP(); // telling the class to use SMTP
				$mail->Host       = "mail.aeplitsolution.co.in"; // SMTP server
				//$mail->SMTPDebug  = 2;                     // enables SMTP debug information (for testing)
														   // 1 = errors and messages
														   // 2 = messages only
				$mail->SMTPAuth   = true;                  // enable SMTP authentication
				$mail->Username   = "test@aeplitsolution.co.in"; // SMTP account username
				$mail->Password   = "Nopass123";        // SMTP account password				
				$mail->From	      ='test@aeplitsolution.co.in';
                $mail->FromName   = "Vriddhi Landmart";	// Header Name 			
				$mail->AddReplyTo($to,"Vriddhi Landmart");

				$mail->Subject    = 'Vriddhi Landmart Password Change';				
				$mail->AltBody    = "To view the message, please use an HTML compatible email viewer!"; // optional, comment out and test				
				$mail->Body= $body;				
				
				$mail->AddAddress($to, "Vriddhi Landmart");
				$mail->AddAddress("test@aeplitsolution.co.in", "Techpromind");					
				
				if(!$mail->Send()) {
				  echo "Mailer Error: " . $mail->ErrorInfo;
				} 
	}*/
    require_once 'lib/$_profile.inc.php';
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <?php require_once 'lib/$_title.php'; ?>
	
	<script type="text/JavaScript" src="js/sha512.js"></script> 
    <script type="text/JavaScript" src="js/forms.js"></script>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

       <?php require_once 'lib/$_header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php require_once 'lib/$_menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          <h1>
            User Profile
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Dashboard</a></li>
            <li class="active">User profile</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">

          <div class="row">
            <div class="col-md-3">

              <!-- Profile Image -->
              <div class="box box-primary">
                <div class="box-body box-profile">
                  <img class="profile-user-img img-responsive img-circle" src="dist/img/avatar5.png" alt="User profile picture">
                  <h3 class="profile-username text-center"><?php echo $loginname; ?></h3>
                  <p class="text-muted text-center">Administrator</p>
                  <a href="#" class="btn btn-primary btn-block"><b>Admin</b></a>
                </div><!-- /.box-body -->
              </div><!-- /.box -->


            </div><!-- /.col -->
            <div class="col-md-9">
              <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                  <li class="active"><a href="#activity" data-toggle="tab">Settings</a></li>
				  <li><?php
                        if (!empty($error_msg)) {
                            echo "<label style=\"color:red;\">".$error_msg."</label>";
                            }
        ?></li>
                </ul>
                <div class="tab-content">
                  <div class="active tab-pane" id="activity">
                    <!-- Post -->
                   
                  <div class="tab-pane" id="settings">
                    <form class="form-horizontal" method="post" name="registration_form" action="profile">
					<input type="hidden" name="up_id" value="<?php echo $loginid; ?>" />
					
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="name" id="name" placeholder="Full Name" value="<?php echo $loginname; ?>" autocomplete="off" />
                        </div>
                      </div>
                     
                      <div class="form-group">
                        <label for="inputName" class="col-sm-2 control-label">User ID</label>
                        <div class="col-sm-10">
                          <input type="text" class="form-control" name="username" id="username" placeholder="User ID" value="<?php echo $user_name; ?>" autocomplete="off" />
                        </div>
                      </div>
					 
                      <div class="form-group">
                        <label for="inputSkills" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                          <input type="Password" class="form-control" name="password" id="pass" placeholder="Password" />
                        </div>
                      </div>
					  
                      <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                          <button type="submit" class="btn btn-primary" name="updt" onclick="return regformhash(this.form,
                                   this.form.name,
								   this.form.username,
                                   this.form.pass);">Submit</button>
                        </div>
                      </div>
                    </form>
                  </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
              </div><!-- /.nav-tabs-custom -->
            </div><!-- /.col -->
          </div><!-- /.row -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
        <?php require_once 'lib/$_footer.php'; ?>
    </div><!-- ./wrapper -->

	
	<div class="control-sidebar-bg"></div>
    <!-- jQuery 2.1.4 -->
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
  </body>
</html>
<?php 
	}
	else
	{  
        session_destroy();
        echo "<script type=\"text/javascript\">alert('Update successful. Please Login Again!!'); window.location='index';</script>";
		
	}
	$link->close();
?>