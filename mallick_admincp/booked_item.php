<?php
    require_once 'lib/$_functions.php'; 
	sec_session_start();
	if (login_check($link) == true)
	{
		require_once 'lib/_reduse.php'; 
		extract($_POST);
		if(isset($_GET['book'])){
			$sql="update `add_to_cart` set `status`=1 ,delivery_date ='".date('Y-m-d H:i:s')."' where `id`='".$_GET['book']."'";
			if($link->query($sql)) echo "<script>alert('successfull');window.location.href='booked_item';</script>";
			else echo "<script>alert('Not Successfull')</script>";
		}
		
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
 <?php require_once 'lib/$_title.php'; ?>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
    <link rel="stylesheet" href="js/mycss.css" />
	
	<!-- DataTables -->
    <link rel="stylesheet" href="plugins/datatables/dataTables.bootstrap.css" />
    <!-- Select2 -->
    <link rel="stylesheet" href="plugins/select2/select2.min.css" />
    <!-- Theme style -->
    <link rel="stylesheet" href="dist/css/AdminLTE.min.css" />
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.8.2.js"></script>-->
<link rel="stylesheet" href="css/stylesheet-image-based.css" />
<script src="//cdn.ckeditor.com/4.7.3/basic/ckeditor.js"></script>
  </head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">

   <?php require_once 'lib/$_header.php'; ?>
  <!-- Left side column. contains the logo and sidebar -->
   <?php require_once 'lib/$_menu.php'; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       All Booked Item List
        <small></small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-pencil"></i> All Booked Item List</a></li>
        <li class="active"></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Info boxes -->

        <div class="row">
			<div class="col-md-12">
			    <div class="box">
				    <div class="box-header with-border">
				        
						<div class="box-tools pull-right">
							<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
						</div>
					</div><!-- /.box-header -->
						
					<div class="box-body">
					
                    <table class="table table-bordered table-striped" id="example1">
                    <thead>
					    <tr>
						    <th>#</th>
						   	<th>Client Name</th>
						   	<th>Phone</th>
						   	<th>Email</th>
						    <th>Product Name</th>
						    <th>Category</th>
						    <th>Quantity</th>
						    <th>Pincode</th>
						    <th>Booking Date & Time</th>
						    <th>Action</th>
						   
						</tr>
                    </thead>
                    <tbody>
					<?php 
						$cont = 1;
					    $stmt = $link->prepare("SELECT * FROM `add_to_cart` WHERE `status`='0' ORDER BY `id` DESC");
						$stmt->execute();
						$result = $stmt->get_result();
						while($data = $result->fetch_assoc()){
							$sql=$link->query("SELECT * FROM `user_table` WHERE `email`='".$data['username']."'");
							$row=@mysqli_fetch_assoc($sql);

							
					?>
						<tr>
							<td><?php echo $cont; ?></td>
							
							<td><?php echo $row['username']; ?></td>
							<td><?php echo $row['phno']; ?></td>
							<td><?php echo $data['username']; ?></td>
							<td><?php echo $data['name']; ?></td>
							<td><?php echo $data['cat']; ?></td>
							<td><?php echo $data['quantity']; ?></td>
							<td><?php echo $data['pin_code']; ?></td>
							<td><?php echo $data['date_of_book']; ?></td>
							<td><a href="booked_item?book=<?php echo $data['id']?>" class="btn btn-info" >Delivered</a></td>
							
							
						</tr>
                    <?php					
							
					    $cont++;
						}
					?>
                    </tbody>
					
                    <!--<tfoot>
                      <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                      </tr>
                    </tfoot>-->
                    </table>
					</div><!-- /.box-body -->
					  
			    </div><!-- /.box -->
            </div> <!-- /.col -->
        </div> <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php require_once 'lib/$_footer.php'; ?>
</div>
<div class="control-sidebar-bg"></div>

    </div><!-- ./wrapper -->
    
 <!-- jQuery 2.1.4 -->
 
    <script src="plugins/jQuery/jQuery-2.1.4.min.js"></script>
	
    <!-- Bootstrap 3.3.5 -->
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <!-- Select2 -->
    <script src="plugins/select2/select2.full.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
	
    <!-- SlimScroll 1.3.0 -->
    <script src="plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- iCheck 1.0.1 -->
    <script src="plugins/iCheck/icheck.min.js"></script>
    <!-- FastClick -->
    <script src="plugins/fastclick/fastclick.min.js"></script>
   <!-- DataTables -->
    <script src="plugins/datatables/jquery.dataTables.min.js"></script>
    <script src="plugins/datatables/dataTables.bootstrap.min.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/app.min.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <!-- Page script -->
    <script>
      $(function () {
        //Initialize Select2 Elements
        $(".select2").select2();
      });
    //iCheck for checkbox and radio inputs
      $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
      });
    </script>
    <script>
      $(function () {
        $("#example1").DataTable();
      });
    </script>
   <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.1.0/jquery.form.min.js"></script>
	---------------------------------------Insert Data---------------------------------------------------------------------
	<script>
	    function submitform(obj){
			var name = $('#name').val();
			if(name == ''){
				alert("Name is missing");
			}else{
				$(obj).ajaxSubmit({
					success:successForm
				});
			}
			
			return false;
		}
		
		function successForm(result){
		    if(result==1){
				$('.form')[0].reset();
				$('.error').html('<i style=\"color: #05A3D3;\">Successfully Insert</i>');
			}else{
				$('.error').html('<i style=\"color:#d32205;\">Unsuccessfully Insert</i>');
			}
		}
	</script>
	
	<!-----------------------------------------Insert Data--------------------------------------------------------------------->
	<!-----------------------------------------Fetch Data---------------------------------------------------------------------
	<script type="text/javascript">
		$(document).ready(function(){
			setInterval(function(){
				$('#show').load('customer_master_data.php')
			}, 500);
		});
	</script>
	<!-----------------------------------------Fetch Data--------------------------------------------------------------------->
    <!-----------------------------------------Delete Data--------------------------------------------------------------------->
	<script type="text/javascript">
	    $(function(){
			$(".delete").click(function(){
				var element = $(this);
				var userid = element.attr("id");
				var info = 'id=' + userid;
				if(confirm("Are you sure want to delete?")){
					$.ajax({
						url: 'delete_mobile_list.php',
						type: 'post',
						data: info,
						success: function(){
							
						}
					});
					$(this).parent().parent().fadeOut(1500, function(){
						$(this).remove();
					});
				};
				return false;	
			});
		});
	</script>
	
	<!-----------------------------------------Delete Data--------------------------------------------------------------------->
  </body>
</html>
<?php 
	}
	else
	{  
        @mysqli_free_result($LoginData);
		session_destroy();
		header("Location:index");
		
	}
	$link->close();
?>