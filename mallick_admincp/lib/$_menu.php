<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <ul class="sidebar-menu">
            <li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'home.php')
				    echo "class=\"active treeview\"";
			    elseif(basename($_SERVER['PHP_SELF']) == 'user_login.php')
				    echo "class=\"active treeview\"";
			    elseif(basename($_SERVER['PHP_SELF']) == 'profile.php')
				    echo "class=\"active treeview\"";
				else
				    echo "class=\"treeview\"";
?> >
              <a href="#">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'home.php') echo "class=\"active\""; ?>><a href="home"><i class="fa fa-circle-o"></i> Dashboard</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'profile.php') echo "class=\"active\""; ?>><a href="profile"><i class="fa fa-circle-o"></i> Profile</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'user_login.php') echo "class=\"active\""; ?>><a href="user_login"><i class="fa fa-circle-o"></i> Login Date Time</a></li>
              </ul>
            </li>
			
			<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'cms_view.php')
				    echo "class=\"active treeview\"";
				elseif(basename($_SERVER['PHP_SELF']) == 'short_dis.php')
				    echo "class=\"active treeview\"";
				elseif(basename($_SERVER['PHP_SELF']) == 'about_us.php')
				    echo "class=\"active treeview\"";
				elseif(basename($_SERVER['PHP_SELF']) == 'our_team_mem.php')
				    echo "class=\"active treeview\"";	
				elseif(basename($_SERVER['PHP_SELF']) == 'index_tab1.php')
				    echo "class=\"active treeview\"";	
				else
				    echo "class=\"treeview\"";
?> >
              <a href="#">
                <i class="fa fa-pencil"></i> <span>CMS View</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			    <li <?php if(basename($_SERVER['PHP_SELF']) == 'cms_view.php') echo "class=\"active\""; ?>><a href="cms_view"><i class="fa fa-circle-o"></i> CMS View</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'about_us.php') echo "class=\"active\""; ?>><a href="about_us"><i class="fa fa-circle-o"></i> About Us</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'our_team_mem.php') echo "class=\"active\""; ?>><a href="our_team_mem"><i class="fa fa-circle-o"></i> Our Team Members</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'short_dis.php') echo "class=\"active\""; ?>><a href="short_dis"><i class="fa fa-circle-o"></i> Short Description</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'index_tab1.php') echo "class=\"active\""; ?>><a href="index_tab1"><i class="fa fa-circle-o"></i> Index Tab-I</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'index_tab2.php') echo "class=\"active\""; ?>><a href="index_tab2"><i class="fa fa-circle-o"></i> Index Tab-II</a></li>
              </ul>
            </li>
			
			<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'add_mobile.php')
				    echo "class=\"active treeview\"";
				elseif(basename($_SERVER['PHP_SELF']) == 'all_mobile_list.php')
				    echo "class=\"active treeview\"";
				else
				    echo "class=\"treeview\"";
?> >
              <a href="#">
                <i class="fa fa-pencil"></i> <span>Mobile Product</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			    <li <?php if(basename($_SERVER['PHP_SELF']) == 'add_mobile.php') echo "class=\"active\""; ?>><a href="add_mobile"><i class="fa fa-circle-o"></i> Add New Mobile</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'all_mobile_list.php') echo "class=\"active\""; ?>><a href="all_mobile_list"><i class="fa fa-circle-o"></i> Add Mobile Product</a></li>
              </ul>
            </li>
			
			<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'add_other_product.php')
				    echo "class=\"active treeview\"";
				elseif(basename($_SERVER['PHP_SELF']) == 'all_other_product.php')
				    echo "class=\"active treeview\"";
				else
				    echo "class=\"treeview\"";
?> >
              <a href="#">
                <i class="fa fa-pencil"></i> <span>Other Product</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
			    <li <?php if(basename($_SERVER['PHP_SELF']) == 'add_other_product.php') echo "class=\"active\""; ?>><a href="add_other_product"><i class="fa fa-circle-o"></i> Add New Product</a></li>
                <li <?php if(basename($_SERVER['PHP_SELF']) == 'all_other_product.php') echo "class=\"active\""; ?>><a href="all_other_product"><i class="fa fa-circle-o"></i> Add Product Item</a></li>
              </ul>
            </li>
			

				<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'clients.php')
				    echo "class=\"active treeview\"";	
				else	
				    echo "class=\"treeview\"";
?>>
			    <a href="#">
                    <i class="fa fa-handshake-o"></i> <span>Clients</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
				 <ul class="treeview-menu">
				   <li <?php if(basename($_SERVER['PHP_SELF']) == 'clients.php') echo "class=\"active\""; ?>><a href="clients"><i class="fa fa-circle-o"></i> Clients</a></li>
			     </ul>
			</li>



<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'booked_item.php'||basename($_SERVER['PHP_SELF']) == 'delivered_item.php')
				    echo "class=\"active treeview\"";	
				else	
				    echo "class=\"treeview\"";
?>>
			    <a href="#">
                    <i class="fa fa-shopping-cart"></i> <span>Booked Item</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
				 <ul class="treeview-menu">
				   <li <?php if(basename($_SERVER['PHP_SELF']) == 'booked_item.php') echo "class=\"active\""; ?>><a href="booked_item"><i class="fa fa-circle-o"></i> Booked Item</a></li>
			     <li <?php if(basename($_SERVER['PHP_SELF']) == 'delivered_item.php') echo "class=\"active\""; ?>><a href="delivered_item"><i class="fa fa-circle-o"></i> Delivered Item</a></li>
			   
			     </ul>
			       
			</li>

						
			<li <?php 

			    if(basename($_SERVER['PHP_SELF']) == 'contact.php')
				    echo "class=\"active treeview\"";	
				else	
				    echo "class=\"treeview\"";
?>>
			    <a href="#">
                    <i class="fa fa-edit"></i> <span>Contact Us</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
				 <ul class="treeview-menu">
				   <li <?php if(basename($_SERVER['PHP_SELF']) == 'contact.php') echo "class=\"active\""; ?>><a href="contact"><i class="fa fa-circle-o"></i> Contact US</a></li>
			     </ul>
			</li>
</ul>
        </section>
        <!-- /.sidebar -->
      </aside