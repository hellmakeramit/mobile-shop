<?php
/**
 * Created by PhpStorm.
 * User: amit
 * Date: 7/22/2018
 * Time: 1:34 AM
 */
require_once"lib/config.php";
require_once 'mallick_admincp/lib/$_config.php';
?>
<!DOCTYPE html>
<html>
<head>
    <title>Welcome To Mallick Mobile</title>
    <!--/tags -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Elite Shoppy Responsive web template, Bootstrap Web Templates, Flat Web Templates, Android Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyEricsson, Motorola web design" />
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
        function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!--//tags -->
    <link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
    <link href="css/font-awesome.css" rel="stylesheet">
    <link href="css/easy-responsive-tabs.css" rel='stylesheet' type='text/css'/>
    <!-- //for bootstrap working -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Lato:400,100,100italic,300,300italic,400italic,700,900,900italic,700italic' rel='stylesheet' type='text/css'>

    <style>
        /*-- banner --*/
        <?php
            $cat = 'slider';
            $i = 1;
            $stmt = $link->prepare("SELECT `img` FROM `cms_view` WHERE `cat` =? ORDER BY `id` ASC");
            $stmt->bind_param('s', $cat);
            $stmt->execute();
            $result = $stmt->get_result();
            while($img = $result->fetch_assoc()){
                if($i == 1){
        ?>
        .carousel .item{
            background:-webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:-moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:-ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background-size:cover;
        }
        <?php
                }else{
        ?>
        .carousel .item.item<?php echo $i; ?>{
            background:-webkit-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:-moz-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:-ms-linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background:linear-gradient(rgba(23, 22, 23, 0.2), rgba(23, 22, 23, 0.5)), url(<?php echo $img['img']; ?>) no-repeat;
            background-size:cover;
        }
        <?php
                }
                $i++;
            }
        ?>
    </style>

</head>
<body>
<!-- header -->
<?php require_once 'lib/_header.php';

if($_SERVER['REQUEST_METHOD']!='POST') echo"<script>history.back();</script>";
if(checkSignin($link)!=true)echo"<script>history.back();</script>";

foreach ($_POST as $key => $value) {
    if ((preg_match("/shipping/i", $key))&&(!preg_match("/shipping2/i", $key))) {
    $uid[]=$value;
}
 if (preg_match("/quantit/i", $key)) {
    $quan[]=$value;    
}   
}
?>
<!-- //banner-top -->
<!------------------ Banner Data---------------->
<?php //require_once "index_properties/banner.php";?>
<!------------------ End Of Banner Data----------->
<!----------------------- Selected Head Phone And Mobiles--------------------->
<?php // require_once "index_properties/attractive_property.php";?>
<!----------------------- End Head Phone And Mobiles--------------------->
<!--------------------- Shedule Properties------------------------>

<?php // require_once "index_properties/schedule_bottom.php";?>
<!--------------------- End Shedule Properties------------------------>
<!-- banner-bootom-w3-agileits -->
<br>
<br>
<br>
<?php // require_once "index_properties/offer_zone.php";?>

<?php// require_once "index_properties/new_arrivals.php";?>
<!-- /we-offer -->
<div class="single-pro">
    <form method="post" action="added_to_cart">
    <?php
    $charge=0;
    $i=0;
    foreach ($uid as $key => $value) {
        # code...
    
            if (preg_match("/mob/i", $value)) {
            $str=str_replace("mob_", "", $value);
            $sql=$link->query("SELECT * FROM  `all_mobile_item` WHERE `id`='".$str."'");
            $valmob=mysqli_fetch_assoc($sql);
            $charge+=$valmob['charge'];
    ?>
    
<div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">
                            <div class="men-thumb-item">
                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                                <span style="margin-right: 227px;" onclick="remove(this)" class="product-new-top">Remove</span><span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                </div>
                             <div class="form-group">   <input type="hidden" name="uid[]" value="<?php echo $valmob['id']?>">
                                <input type="hidden" name="name[]" value="<?php echo $valmob['name']?>">
                                <input type="hidden" name="cat[]" value="Mobile">
                                <label>Quantity: </label>
                                <input type="number" name="quantity[]" style="width: 100px; margin-left: 200px;" class="form-control" value="<?php echo $quan[$i]?>">
                            </div>
                            </div>
                        </div>
                    </div><!-- col-md-3 product-men -->

            <?php
              }
              if (preg_match("/other/i", $value)) {
            $str=str_replace("other_", "", $value);
            $sql=$link->query("SELECT * FROM  `all_other_item` WHERE `id`='".$str."'");
            $valmob=mysqli_fetch_assoc($sql);
            $charge+=$valmob['charge'];

    ?>
    
<div class="col-md-3 product-men">
                        <div class="men-pro-item simpleCart_shelfItem">

                            <div class="men-thumb-item">

                                <img src="<?php echo $valmob['img1']; ?>" alt="" class="pro-image-front" style="height: 350px">
                                <img src="<?php echo $valmob['img2']; ?>" alt="" class="pro-image-back" style="height: 350px">
                                <div class="men-cart-pro">
                                    <div class="inner-men-cart-pro">
                                        <a href="#" class="link-product-add-cart">Quick View</a>
                                    </div>
                                </div>
                               <span style="margin-right: 227px;" onclick="remove(this)" class="product-new-top">Remove</span><span class="product-new-top">New</span>

                            </div>
                            <div class="item-info-product ">
                                <h4 style="color:#f00;"><?php echo $valmob['name']; ?></h4>
                                <div class="info-product-price">
                                    <?php

                                    ?>
                                    <span class="item_price"><?php echo $valmob['o_price']!=''? $valmob['o_price']."/- Rs":$valmob['c_price']."/- Rs" ?></span>
                                    <del><?php echo $valmob['o_price']!=''? $valmob['c_price']."/- Rs":false?></del>
                                
                                </div>
                                <div class="form-group">   <input type="hidden" name="uid[]" value="<?php echo $valmob['id']?>">
                                <input type="hidden" name="name[]" value="<?php echo $valmob['name']?>">
                                <input type="hidden" name="cat[]" value="<?php echo $valmob['category'];?>">
                                <label>Quantity: </label>
                                <input type="number" name="quantity[]" style="width: 100px; margin-left: 200px;" class="form-control" value="<?php echo $quan[$i]?>">
                            </div>

                            </div>
                        </div>
                    </div><!-- col-md-3 product-men -->

            <?php
              }
              $i++;
                }
            ?>
            <div class="clearfix"></div>
            <center>
            <div class="description">
                        <h5>Check delivery, payment options and charges at your location</h5>
                        <input type="text" name="pincode" placeholder="Enter pincode" id="txt_val" maxlength="6" required />
                        <input type="button" style="
    color: #fff;
    font-size: 16px;
    background: #000000;
    border: none;
    outline: none;
    padding: 7px 17px 9px;
    letter-spacing: 2px;
    text-transform: uppercase;
    -webkit-transition: 0.5s all;
" value="Check" id="check_pin" /><br />
                        <span id="print_pin" style="font-size: 14px; color: #0eb51bfc; font-family: cursive; font-weight: 600;"></span>
                    </div></center>
            <div class="col-md-12"><center><button style="margin-top: 29px;" type="submit" name="save_data" class="btn btn-success"> Save</button></center></div>
        </form>
</div>
<div class="clearfix"></div>
<!-- //we-offer -->
<!--/grids-->
<?php require_once 'lib/_grids.php'; ?>
<!--grids-->
<!-- footer -->
<?php require_once 'lib/_footer.php'; ?>

<a href="#home" class="scroll" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>

<!-- js -->
<script type="text/javascript" src="js/jquery-2.1.4.min.js"></script>
<!-- //js -->
<script src="js/modernizr.custom.js"></script>
<!-- Custom-JavaScript-File-Links -->
<!-- cart-js -->
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $("#check_pin").click(function(event){
            var txt_val = $("#txt_val").val();
            var pin = '<?php echo $pin; ?>';
            var charges = '<?php echo $charge; ?>';
            if(txt_val == ""){
                alert("Please Enter Pin Code")
            }else{
                if(pin == txt_val){     
                    $("#print_pin").html("Delivery Charge Free");
                }else{
                    var text1 = '<i class="fa fa-rupee"></i> '+charges+' Delivery Charge Extra';
                    $("#print_pin").html(text1);
                }
            }
        });
    });
</script>
<script src="js/minicart.min.js"></script>
<script>
    // Mini Cart
    paypal.minicart.render({
        action: '#'
    });

    if (~window.location.search.indexOf('reset=true')) {
        paypal.minicart.reset();
    }


    function remove(x){
        x.closest('.col-md-3').remove();
    }
</script>

<!-- //cart-js -->
<!-- script for responsive tabs -->
<script src="js/easy-responsive-tabs.js"></script>
<script>
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true,   // 100% fit in a container
            closed: 'accordion', // Start closed if in accordion view
            activate: function(event) { // Callback function if tab is switched
                var $tab = $(this);
                var $info = $('#tabInfo');
                var $name = $('span', $info);
                $name.text($tab.text());
                $info.show();
            }
        });
        $('#verticalTab').easyResponsiveTabs({
            type: 'vertical',
            width: 'auto',
            fit: true
        });
    });
</script>
<!-- //script for responsive tabs -->
<!-- stats -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="js/jquery.waypoints.min.js"></script>
<script src="js/jquery.countup.js"></script>
<script>
    $('.counter').countUp();
</script>
<!-- //stats -->
<!-- start-smoth-scrolling -->
<script type="text/javascript" src="js/move-top.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/minicart/3.0.6/minicart.js"></script>

<script type="text/javascript" src="js/jquery.easing.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>

<script>
    $(document).ready(function(){
        // $("button").click(function(){html()
        $(".minicart-submit").html("Book Now");
        $(".minicart-submit").removeClass("minicart-submit").addClass("btn btn-info");

        // });
    });
</script>
<!-- here stars scrolling icon -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
            var defaults = {
            containerID: 'toTop', // fading element id
            containerHoverID: 'toTopHover', // fading element hover id
            scrollSpeed: 1200,
            easingType: 'linear'
            };
        */
        //  $( ".submit" ).replaceWith( "<div>" + $( ".submit" ).text() + "</div>" );
        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<script>



</script>
<!-- //here ends scrolling icon -->


<!-- for bootstrap working -->
<script type="text/javascript" src="js/bootstrap.js"></script>
<?php require_once 'lib/_all_list.php'; ?>
</body>

<!-- Mirrored from p.w3layouts.com/demos_new/template_demo/20-06-2017/elite_shoppy-demo_Free/143933984/web/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 28 Oct 2017 12:26:54 GMT -->
</html>

